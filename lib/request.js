'use strict';
class RequestPromise{
	constructor(){
		this.promise=Promise.resolve();
		this.resolve=function(){};
		this.reject=function(){};
	}
	/**
	 * New promise
	 * 
	 * @param {function || boolean} fn
	 * @param {object} data
	 */
	await(){
		if(arguments.length<1) return this.promise;
		var self=this,
		pass_args=[],
		fn=arguments[0],
		pass_resolve=false,
		pass_this=void 0,
		i_start=1;
		if(typeof fn=='object' && arguments.length>1){
			pass_this=fn;
			fn=fn[arguments[1]];
			i_start++;
		}
		if(arguments.length===1 && typeof arguments[0]!='function'){
			self.promise=self.promise.then(() => {
				return new Promise((resolve, reject) => {
					resolve(fn);
				});
			});
			return self.promise;
		}
		if(typeof fn!='function') return this.promise;
		for(let i=i_start; i<arguments.length; i++){
			if((arguments[i].name==='Promise' || arguments[i] && arguments[i].constructor.name=='Promise') && i>=arguments.length-1){
				pass_resolve=arguments[i];
				break;
			}
			pass_args.push(arguments[i]);
		}
		self.promise=self.promise.then(function(){})
		.catch(function(){})
		.then(function(){
			return new Promise(function(resolve, reject){
				self.resolve=resolve;
				self.reject=reject;
				if(pass_resolve.name==='Promise' || pass_args.length<1/*  || fn.length>pass_args.length */) pass_args.push(resolve, reject);
				var cb=fn.apply(pass_this, pass_args);
				if(pass_resolve && pass_resolve.constructor.name=='Promise'/*  || fn.length<=pass_args.length */) resolve(cb);
			});
		});
		return self.promise;
	}
	then(fn){
		this.await().then(fn);
	}
};

module.exports = RequestPromise;