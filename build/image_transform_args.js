function convertirNumero(img, valor, medida){
	return parseFloat(valor);
}
function convertirBooleano(img, valor, medida){
	return eval(valor)?true:false;
}
function convertirMedida(numero, valor, medida){
	if(valor=='auto') return valor;
	valor = parseFloat(valor);
	return filtros_medidas[medida](valor, numero);
}
function convertirMedidaWidth(img, valor, medida){
	return convertirMedida(img.bitmap.width, valor, medida);
}
function convertirMedidaHeight(img, valor, medida){
	return convertirMedida(img.bitmap.height, valor, medida);
}
var arrayConvertirMedida = [
	convertirMedidaWidth,
	convertirMedidaHeight
];
const filtros_medidas = {
	'': function(v, n){
		return this.px(v, n);
	},
	'px': function(v){
		return v;
	},
	'%': function(v, n){
		return n*(v/100);
	}
};
module.exports = {
	"background": [
		function(img, valor, medida){
			switch(valor){
				case 'black':
					valor=0x000000ff;
					break;
				case 'white':
					valor=0xffffffff;
					break;
			}
			return valor;
		}
	],
	"resize": arrayConvertirMedida,
	"contain": arrayConvertirMedida,
	"cover": arrayConvertirMedida,
	"crop": [
		...arrayConvertirMedida,
		...arrayConvertirMedida
	],
	"quality": [
		function(img, valor, medida) {
			return parseInt(valor);
		},
	],
	"greyscale": true,
	"write": false, "clone": false,
};