#!/usr/bin/env node
process.setMaxListeners(0);
process.emitWarning = (warning, ...args) => {
	if (args[0] === 'ExperimentalWarning') return;
	if (args[0] && typeof args[0] === 'object' && args[0].type === 'ExperimentalWarning') return;
	// return emitWarning(warning, ...args);
};
// modulos de archivos
const fs = require('fs'),
      path = require('path');
// modulos de cli
const cli = require('command-line-args');
// require archivos locales
const log = require('../lib/log.js'),
	  Request = require('../lib/request.js');
const request = new Request();
const transform_image = require('./image_transform.js');
//
//
// command line args
var default_output = '-edited';
const cli_args = [
	{ // pruebas de funcionamiento
		name: 'debug',
		type: Boolean,
		defaultValue: false
	},
	{ // buscar en rutas, carpetas o utilizar archivos
		name: 'input', // utils-image --input "ruta/carpeta_1" "ruta/carpeta_2" "ruta_archivo_1.png" "ruta_archivo_2.jpg" "ruta_x" ...
		alias: 'i',
		type: String,
		multiple: true,
		defaultValue: '.' // si no se definen directorios buscara en la carpeta actual
	},
	{ // buscar en subcarpetas
		name: 'recursive', // utils-image ... --recursive
		alias: 'r',
		type: Boolean
	},
	{ // reemplazar archivo
		name: 'replace', // utils-image ... --replace
		type: Boolean
	},
	{ // aplicar filtros con Jimp https://www.npmjs.com/package/jimp
		name: 'transform', // utils-image ... --transform "invert" "sepia" "scale(0.5)" "flip(true, false)" ["...filtro"]
		alias: 't',
		type: String,
		multiple: true,
		defaultValue: []
	},
	{ // cambiar la extension al archivo, formatos admitidos por Jimp https://www.npmjs.com/package/jimp
		name: 'extension', // utils-image ... --extension "jpg"|"png"|"bmp"
		alias: 'x',
		type: String
	},
	{ // establecer a donde guardara los archivos. Si se define vacio el archivo sera reemplazado
		name: 'output', // utils-image ... --output ""|"otra_ruta"
		alias: 'o',
		type: String,
		defaultValue: '.' // utilizar la ruta {default_output} relativa a cada archivo
	},
];
const cli_vars = cli(cli_args, {
	partial: true
});
//
function comprobarArchivo(file){
	// extension
	var extension = path.extname(file).slice(1); // obtener extension sin punto
	// si la extension coincide con la lista permitida
	if(['jpg', 'jpeg', 'png', 'bmp'].indexOf(extension.toLowerCase())<0) return;
	// nombre & extension
	var name = path.basename(file, '.'+extension); // obtener nombre y quitar la extension
	var dirname = path.dirname(file);
	if(name.charAt(0) == '.' || path.basename(dirname) == default_output || name.indexOf(default_output)>=0) return; // ignorar archivos ocultos y autogenerados por utils-image
	var original_extension = extension; // extension original antes de cambiar a obtenida por cli
	if(cli_vars.extension) extension = cli_vars.extension.toLowerCase(); // extension obtenida por cli
	if(extension == 'JPEG') extension = 'JPG'; // cambiar JPEG a JPG
	if(extension == 'jpeg') extension = 'jpg'; // cambiar jpeg a jpg
	// transformar y guardar
	request.await(function(resolve, reject){
		transform_image(file, [extension, original_extension], cli_vars.transform)
		.then(function(imagen){
			var [img, imagenFiltrada] = imagen;
			if(!imagenFiltrada) return reject();
			var output_folder,
			output_file;
			// directorio de salida
			if(cli_vars.output){
				if(!fs.existsSync(cli_vars.output) || cli_vars.output == '.') output_folder = path.resolve(dirname+'/'+cli_vars.output);
				else output_folder=cli_vars.output;
			}
			else{
				output_folder = dirname;
			}
			if(!cli_vars.replace && output_folder != dirname && !fs.existsSync(output_folder)) fs.mkdirSync(output_folder, {
				recursive: true
			});
			// archivo final
			output_file = output_folder+'/'+name+(!cli_vars.replace && output_folder == dirname ? default_output : '')+'.'+extension;
			// console.log(output_folder, name);
			// reemplazar archivo (renombrar para sobreescribir imagen)
			if(cli_vars.replace) fs.renameSync(file, output_file);
			// guardar
			if(!cli_vars.debug) fs.writeFileSync(output_file, img);
			resolve();
		});
	})
	.then(function(){
		log('{{success}}'+file);
	})
	.catch(function(){
		log('{{info}}'+file);
	})
	;
	// - guardar stream a archivo
}
// leer lista de archivos
function comprobarRuta(file, principal){
	file = path.resolve(file);
	if(!fs.existsSync(file)){
		if(principal){
			log('{{warn}} La ruta no existe\n'+file);
		}
		return;
	}
	var stats = fs.statSync(file);
	if(stats.isDirectory()){
		// si es principal o recursivo buscar dentro de la carpeta mas archivos
		if(principal || cli_vars.recursive){
			let files = fs.readdirSync(file);
			for(let i=0; i<files.length; i++){
				comprobarRuta(file+'/'+files[i]);
			}
		}
		else return; // cancelar lectura si no es recursivo
	}
	else{
		comprobarArchivo(file);
	}
}
for(let i=0; i<cli_vars.input.length; i++){
	comprobarRuta(cli_vars.input[i], true);
}