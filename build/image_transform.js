//
const path = require('path');
// modulos extra
const Jimp = require('jimp');
// require archivos locales
const log = require('../lib/log.js');
//
// corregir filtro
var filtroCorregido = false;
var filtroGuardado = {};
function corregirFiltro(filtro){
	if(filtroCorregido) return;
	filtroCorregido = true;
	for(let i=0; i<filtro.length; i++){
		// filtro
		filtro[i] = filtro[i].match(/(\w*)\(*([\-\w\,\s\.\%]*)\)*/); // separar funcion de los argumentos
		if(filtro[i].length > 3) continue; // no aceptar mas de un parentesis con argumentos
		// funcion
		let fn=filtro[i][1];
		filtroGuardado[fn] = true;
		// argumentos
		let args = filtro[i][2];
		args = args.split(/\s*,\s*/); // separar argumentos
		for(let j=0; j<args.length; j++){
			args[j] = args[j].match(/([\d\.]*)(.*)/); // separar valor de la medida
		}
		// actualizar filtro
		filtro[i][2] = args;
	}
}
// aplicar filtro a imagen
const filtros_argumentos = require('./image_transform_args.js');
function filtrarImagen(img, filtro){
	var fn = filtro[1],
	    args = typeof(filtro[2])=='object'?[...filtro[2]]:filtro[2];
	if(filtros_argumentos[fn] === false || typeof img[fn] != 'function') return; // si el argumento no esta declarado o la imagen no soporta la funcion
	if(typeof filtros_argumentos[fn] == 'object' && args.length < filtros_argumentos[fn].length){
		for(let i=args.length; i<filtros_argumentos[fn].length; i++){
			args[i]=args[args.length-1];
		}
	}
	for(let i=0; i<args.length; i++){
		// valor
		let valor = args[i][1];
		// medida
		let medida = args[i][2] || '';
		let medida_auto = medida.toUpperCase() == 'AUTO'; // si se establece auto como valor
		if(medida_auto) medida='auto';
		if(!valor || (medida == args[i][0] && !medida_auto)){ // establece valor si se confunde con medida cuando valor no es numero
			valor = medida;
			medida = void 0;
		}
		if(valor == void 0 && !medida){ // si el valor y medida no fue establecido
			args[i] = void 0;
			continue;
		}
		// hexadecimal
		// cambiar tipo de dato a valor si es completamente numero o boolean
		if(valor){
			if(valor.substr(0, 2)!='0x' && !isNaN(valor)) valor = parseFloat(valor);
			else if(['false', 'true'].indexOf(valor.toLowerCase()) >= 0){
				valor = eval(valor);
			}
		}
		if(valor==0 && medida && medida.charAt(0)=='x'){
			valor = eval('0'+medida);
			medida = void 0;
		}
		// si hay una funcion personalizada asignada la ejecuta
		if (typeof filtros_argumentos[fn] == 'object') {
			if (typeof filtros_argumentos[fn][i] == 'function') valor = filtros_argumentos[fn][i](img, valor, medida);
		}
		// valor auto o indefinido
		if(valor == 'auto') valor = Jimp.AUTO; // valor auto
		else if(valor === '') valor = void 0;
		//
		args[i] = valor;
	}
	var imagen;
	try{
		imagen = img[fn].apply(img, args); // aplicar manipulacion de imagen con Jimp
	}catch(err){
		log('{{error}}'+err+'\n'+args)
		return;
	}
	return imagen;
}
//
const defaultQuality = 80;
//
module.exports=function(file, extension, filtro_original){
	var file_MIME = extension[0].toUpperCase();
	if(file_MIME=='JPG') file_MIME='JPEG'; // cambiar JPG a JPEG
	// convertir filtros solo una vez
	corregirFiltro(filtro_original);
	var filtro = [...filtro_original];
	//
	return new Promise(function(resolver, rechazar){
		Jimp.read(file)
		.then(function(img){
			var imagenFinal = img,
			imagenActual,
			imagenFiltrada = false;
			for(let i=0; i<filtro.length; i++){
				imagenActual = filtrarImagen(imagenFinal, filtro[i]);
				if(!imagenActual) continue;
				imagenFinal = imagenActual;
				imagenFiltrada = true;
			}
			if(extension[0] != extension[1]) imagenFiltrada = true;
			// ajustar calidad si no se define
			if(imagenFiltrada && file_MIME == 'JPEG' && !filtroGuardado['quality']){ // cambiar calidad si no se define
				imagenFinal = imagenFinal.quality(defaultQuality);
			}
			// convertir imagen Jimp a buffer
			imagenFinal.getBufferAsync(Jimp['MIME_'+file_MIME]).then(function(img){
				resolver([img, imagenFiltrada]);
			});
		})
		.catch(function(err){
			log('{{error}}'+err);
		})
		;
	});
};